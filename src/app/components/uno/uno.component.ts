import { Component, OnInit } from '@angular/core';

import { IUserState } from '../../store/states/userState.interface';
import { ACTION_CAMBIO_NOMBRE, ACTION_CAMBIO_CONECTADO } from '../../store/actions/user.actions';

import { StoreService } from 'src/app/services/store.service';

@Component({
  selector: 'app-uno',
  templateUrl: './uno.component.html',
  styleUrls: ['./uno.component.css']
})
export class UnoComponent implements OnInit {

  nombre: string = '';
  conectado: boolean = false;

  constructor(private _storeService: StoreService) { }

  ngOnInit(): void {
    this._storeService.getState('userState').subscribe(
      (state: IUserState) => {
        this.nombre = state.nombre;
        this.conectado = state.conectado;
      },
      (error) => {
        alert(`Error al cargar los datos de userState: ${error}`);
      },
      () => console.log('Datos de userState cargados con éxito')
    );
  }

  cambiarNombre() {
    this._storeService.updateState(
      {
        type: ACTION_CAMBIO_NOMBRE,
        payload: this.nombre
      }
    )
  }

  cambiarConectado() {
    this._storeService.updateState(
      {
        type: ACTION_CAMBIO_CONECTADO,
        payload: !this.conectado
      }
    )
  }

}
