import { Component, OnInit } from '@angular/core';
import { StoreService } from 'src/app/services/store.service';
import { IUserState } from 'src/app/store/states/userState.interface';

@Component({
  selector: 'app-dos',
  templateUrl: './dos.component.html',
  styleUrls: ['./dos.component.css']
})
export class DosComponent implements OnInit {

  nombre: string = '';
  conectado: boolean = false;

  constructor(private _storeService: StoreService) { }

  ngOnInit(): void {
    this._storeService.getState('userState').subscribe(
      (state: IUserState) => {
        this.nombre = state.nombre;
        this.conectado = state.conectado;
      },
      (error) => {
        alert(`Error al cargar los datos de userState: ${error}`);
      },
      () => console.log('Datos de userState cargados con éxito')
    );
  }

}
