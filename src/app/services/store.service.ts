import { Injectable } from '@angular/core';
import { Store } from '@ngrx/store';
import { IStore } from '../store/config/store.interface';

@Injectable({
  providedIn: 'root'
})
export class StoreService {

  constructor(private _store: Store<any>) { }

  // Método para obtener un estado del Store de la aplicación
  getState(state: string) {
    return this._store.select(state);
  }

  // Método para despachar acciones que sean escuchadas por los reducers
  // y actualicen el estado de la aplicación
  updateState(action:any) {
    this._store.dispatch(
      {
        type: action.type,
        payload: action.payload
      }
    )
  }
}
