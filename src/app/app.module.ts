import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { StoreModule } from '@ngrx/store';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';


import { AppComponent } from './app.component';
import { RootReducer } from './store/reducers/rootReducer';
import { UnoComponent } from './components/uno/uno.component';
import { DosComponent } from './components/dos/dos.component';

@NgModule({
  declarations: [
    AppComponent,
    UnoComponent,
    DosComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    // Configuramos el Store con el RootReducer
    StoreModule.forRoot(RootReducer, {}),
    // Configuramos el DevTools de Redux
    StoreDevtoolsModule.instrument(
      {
        maxAge: 5
      }
    )
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
