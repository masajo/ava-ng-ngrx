import { ACTION_CAMBIO_NOMBRE, ACTION_CAMBIO_CONECTADO } from '../actions/user.actions';
import { IUserState } from '../states/userState.interface';

// Estado inicial
const userStateInicial: IUserState = {
  nombre: 'Martín',
  conectado: true
}

export function userReducer(state: IUserState = userStateInicial, action: any): IUserState {
  switch (action.type) {
    // Evaluamos los tipos de acción y devolvemos un nuevo estado
    case ACTION_CAMBIO_NOMBRE:
      return {
        ...state, // replicas con el factor de propagación/spread
        nombre: action.payload,
      }
    case ACTION_CAMBIO_CONECTADO:
      return {
        ...state,
        conectado: action.payload
      }
  }
  return state;
}





