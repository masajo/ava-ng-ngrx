import { ActionReducerMap } from '@ngrx/store';
import { IStore } from '../config/store.interface';
import { userReducer } from './userReducer';

// Root Reducer --> Combinación de Reducers para crear el Store de la aplicación
export const RootReducer: ActionReducerMap<IStore> = {
  // state : reducer asignado
  userState: userReducer
  // ... Aquí metemos más states definidos en IStore asignados a otros reducers
}

