// Intercafe para definir el ascpecto final del Store de la app

import { IUserState } from '../states/userState.interface';


export interface IStore {
  userState: IUserState
  // nombre del estado: interface que define los datos que va a contener
  // ... Añadir más estados al STORE de la aplicación
}
